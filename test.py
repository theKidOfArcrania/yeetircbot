import irc.bot
import os, signal, logging, socket
from functools import partial

logging.getLogger().setLevel(logging.DEBUG)

def pass_raw(sd, conn, evt):
    assert(evt.type == 'all_raw_messages')

    # Consume all data
    sd.setblocking(False)
    while True:
        try:
            sd.read(0x1000)
        except:
            break
    sd.setblocking(True)

    # Pass through data
    sd.send(bytes(evt.arguments[0] + '\n', 'utf8'))

def process_forever(conn, until_disconnect=False, log=logging):
    while not until_disconnect or conn.is_connected():
        try:
            conn.reactor.process_once(.2)
        except KeyboardInterrupt:
            log.error('KeyboardInterrupt')
            break
        except:
            log.error('Top level exception!', exc_info=True)

def disable(c, e):
    return 'NO MORE'

def run_bot(bot):
    signal.signal(signal.SIGHUP, signal.SIG_IGN)
    signal.signal(signal.SIGUSR1, signal.SIG_IGN)
    signal.siginterrupt(signal.SIGHUP, False)
    signal.siginterrupt(signal.SIGUSR1, False)

    conn = bot.connection
    bot._connect()
    disconnect = bot._on_disconnect

    old_sock = conn.socket
    while True:
        s1, s2 = socket.socketpair()
        pid = os.fork()
        if not pid:
            break

        s2.close()
        conn.socket = s1

        conn.add_global_handler('disconnect', disable, -35)

        pass_sig = lambda signum, trace: os.kill(pid, signum)
        signal.signal(signal.SIGHUP, pass_sig)
        signal.signal(signal.SIGUSR1, pass_sig)
        process_forever(conn, True)
        signal.signal(signal.SIGHUP, signal.SIG_IGN)
        signal.signal(signal.SIGUSR1, signal.SIG_IGN)

        conn.remove_global_handler('disconnect', disable)

        _, status = os.waitpid(pid, 0)
        if status & 63 != signal.SIGHUP:
            logging.info('Unknown exit reason!')
            return

        conn.connected = True
        conn.socket = old_sock

    # Passing data back to parent
    s1.close()
    conn.add_global_handler('all_raw_messages', partial(pass_raw, s2))

    # Make sure that any imports start here, to allow full reload of code each
    # time we decide to reload code
    try:
        from yeet.irc.bot import YeetBouncer
        handler = YeetBouncer(bot)
        log = handler.log
    except:
        logging.error('Failed to load yeet bouncer', exc_info=True)
        handler = None
        log = logging.getLogger()

    def hand_hup(signum, trace):
        log.info('Hit a hang-up! Reloading code.')
        bot.connection.socket = None
        try:
            if handler: handler.save()
        except:
            log.error('Failed to save to database!', exc_info=True)

        signal.signal(signal.SIGHUP, signal.SIG_DFL)
        signal.pthread_sigmask(signal.SIG_SETMASK, [])
        os.kill(os.getpid(), signal.SIGHUP)

    def hand_reload_db(signum, trace):
        log.info('Hit a reload! Reloading database.')
        try:
            if handler: handler.load()
        except:
            log.error('Failed to load from database!', exc_info=True)


    signal.signal(signal.SIGHUP, hand_hup)
    signal.signal(signal.SIGUSR1, hand_reload_db)
    process_forever(bot.connection, log=log)
    quit()


print('PID: %s' % os.getpid())
bot = irc.bot.SingleServerIRCBot([('chat.freenode.net', 6667)], 'YeetB0t', 'YeetB0t')
run_bot(bot)
