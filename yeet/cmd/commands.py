import functools, inspect, math

from yeet.util.excs import *

def invoke_cmd(bot, c, e, parts):
    cmd = parts[0] 
    cmds = Command.get_cmds(type(bot))
    if cmd not in cmds:
        raise MissingCommandError()

    e.cmd = [cmd]
    e.cmd_args = parts[1:]
    cmds[cmd].invoke(bot, c, e, parts[1:])
        
class Command(object):
    @staticmethod
    @functools.lru_cache(maxsize=32)
    def get_cmds(cls):
        cmds = {}
        for c in reversed(cls.mro()):
            for cmd in vars(cls).values():
                if isinstance(cmd, Command) and not cmd.parent:
                    cmds[cmd.name] = cmd
        return cmds

    def __init__(self, function, name, **kwargs):
        self.function = function
        self.name = name
        self.parent = kwargs.get('parent', None)
        self.checks = getattr(function, '__cmd_checks', []) + kwargs.get('checks', [])

        spec = inspect.getargspec(function)
        maxargs = len(spec.args) - 3
        if spec.defaults:
            minargs = maxargs - len(spec.defaults)
        else:
            minargs = maxargs

        if spec.varargs:
            maxargs = math.inf

        if maxargs < 0:
            raise TypeError('Expected a function with at least 3 positional parameters')

        self.argct = (minargs, maxargs)

    def invoke(self, bot, c, e, args):
        minargs, maxargs = self.argct
        if not (minargs <= len(args) <= maxargs):
            raise BadArgumentError()

        for chk in self.checks:
            if not chk(c, e):
                raise CheckError

        return self.function(bot, c, e, *args)


class Group(Command):
    def __init__(self, function, name, **kwargs):
        super().__init__(function, name, **kwargs)
        self.subcmds = {}
        self.__onerror = None

    def __add_cmd(self, name, subcmd):
        if name in self.subcmds:
            raise ValueError('Duplicate subcommand')
        self.subcmds[subcmd.name] = subcmd

    def command(self, name, cls=Command, **kwargs):
        def wrapper(*args, **kwargs):
            ret = cls(*args, **kwargs)
            self.__add_cmd(name, ret)
            return ret
        return command(name, cls=wrapper, parent=self, **kwargs)

    def group(self, name, cls=None, **kwargs):
        if cls == None:
            cls = Group
        def wrapper(*args, **kwargs):
            ret = cls(*args, **kwargs)
            self.__add_cmd(name, ret)
            return ret
        return group(name, cls=wrapper, parent=self, **kwargs)

    def invoke(self, bot, c, e, args):
        try:
            minargs, maxargs = self.argct
            cmd = args[0] if args else None
            e.subcmd = cmd
            if super().invoke(bot, c, e, args[0:maxargs]) == 'NO MORE':
                del e.subcmd
                return

            del e.subcmd

            if cmd not in self.subcmds:
                raise MissingCommandError()

            old_cmd = e.cmd
            old_args = e.cmd_args

            try:
                e.cmd = old_cmd + [cmd]
                e.cmd_args = args[1:]
                self.subcmds[cmd].invoke(bot, c, e, args[1:])
            finally:
                e.cmd = old_cmd
                e.cmd_args = old_args
        except:
            if not self.__onerror or self.__onerror(bot, c, e) != 'NO MORE':
                raise

    def on_error(self, fn):
        self.__onerror = fn

def is_user():
    def pred(c, e):
        return e.source_user != None
    return check(pred)

def has_permissions(*chk):
    def pred(c, e):
        need = set(chk)
        eff_acl = e.source_user.effective_acl
        for p in chk:
            if not getattr(eff_acl, p, 0):
                raise MissingPermissionError('need ' + ', '.join(chk))
        return True
            
    return check(pred)

def has_role(role):
    role = Role(role)
    def pred(c, e):
        return role in e.source_user.roles

    return check(pred)

def command(name, cls=Command, **kwargs):
    def wrapper(fn):
        kwargs['function'] = fn
        kwargs['name'] = name
        return cls(**kwargs)
    if callable(name):
        fn = name
        name = fn.__name__
        return wrapper(fn)
    else:
        return wrapper

def group(name, cls=Group, **kwargs):
    return command(name, cls=cls, **kwargs)

def check(predicate):
    def decorator(fn):
        if isinstance(fn, Command):
            fn.checks.append(predicate)
        else:
            if not hasattr(fn, '__cmd_checks'):
                fn.__cmd_checks = []
            fn.__cmd_checks.append(predicate)
        return fn

    return decorator
