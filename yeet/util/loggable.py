import logging
import sys

class Loggable(object):
    def init_config(self, conf):
        ns = conf.ns
        self.__ns = ns

        conf.ns_getbool('logging', 'log_stdout', False)
        conf.ns_get('logging', 'file', None)
        conf.ns_get('logging', 'level', 'DEBUG')

        log = logging.Logger(str(type(self).__name__), logging.NOTSET)
        self.__log = log

        fmt = logging.Formatter('[%(asctime)s] [%(name)s] %(levelname)s: %(message)s')
        if ns.log_stdout:
            hdlr = logging.StreamHandler(sys.stdout)
            hdlr.setLevel(ns.level)
            hdlr.setFormatter(fmt)
            log.addHandler(hdlr)

        if ns.file:
            hdlr = logging.StreamHandler(open(ns.file, 'w'))
            hdlr.setLevel(ns.level)
            hdlr.setFormatter(fmt)
            log.addHandler(hdlr)

    @property
    def log(self):
        return self.__log
