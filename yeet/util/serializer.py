#!/usr/bin/python3

__descs__ = {}

def register_desc(cls, desc):
    __descs__[cls] = desc

def serializable(cls):
    class _Desc(Desc):
        def load(self, os, data):
            return cls.load_obj(os, data)
        def store(self, os, obj):
            if not isinstance(obj, cls):
                raise TypeError()
            return cls.store_obj(os, obj)

    register_desc(cls, _Desc())
    return cls

def fields(flds, cstr=None):
    def wrapper(cls, _flds = flds):
        cstr2 = cstr or cls
        class _Desc(Desc):
            def load(self, os, data):
                return os.load_flds(data, cstr2, **_flds)
            def store(self, os, obj):
                if not isinstance(obj, cls):
                    raise TypeError()
                return os.store_flds(obj, **_flds)

        register_desc(cls, _Desc())
        return cls

    if type(flds) is type:
        return wrapper(flds, flds.__flds__)
    else:
        return wrapper

def singleton(fld, cstr=None):
    def wrapper(cls):
        cstr2 = cstr or cls
        class _Desc(Desc):
            def load(self, os, data):
                return cstr2(data)
            def store(self, os, obj):
                if not isinstance(obj, cls):
                    raise TypeError()
                return getattr(obj, fld)
        register_desc(cls, _Desc())
        return cls
    return wrapper

class Desc(object):
    def load(self, os, data):
        raise ValueError('Abstract method')
    def store(self, os, data):
        raise ValueError('Abstract method')

class PrimType(Desc):
    def load(self, os, data):
        return data
    def store(self, os, data):
        return data

class ListType(Desc):
    def __init__(self, ele_type):
        self.ele_type = ele_type

    def load(self, os, data):
        return [os.load(e, self.ele_type) for e in data]

    def store(self, os, data):
        return [os.store(e, self.ele_type) for e in data]

class Serializer(object):
    def load(self, data, data_type):
        if type(data_type) is type:
            data_type = __descs__.get(data_type)
        return data_type.load(self, data)

    def load_flds(self, _data, _constructor, **_types):
        _data2 = {}
        for name, fld_type in _types.items():
            _data2[name] = self.load(_data[name], fld_type)
        return _constructor(**_data2)

    def store(self, data, data_type=None):
        inval = type(data)
        if data_type == None:
            data_type = __descs__.get(inval, None)
        elif type(data_type) is type:
            inval = data_type
            data_type = __descs__.get(data_type, None)
        elif isinstance(data_type, Desc):
            pass
        if data_type == None:
            raise TypeError('Not a registered type: %s' % inval)

        return data_type.store(self, data)

    def store_flds(self, _data, **_types):
        data = {}
        for name, fld_type in _types.items():
            data[name] = self.store(getattr(_data, name), fld_type)
        return data


_prim = PrimType()
register_desc(int, _prim)
register_desc(str, _prim)
register_desc(dict, _prim)
register_desc(list, ListType(None))

