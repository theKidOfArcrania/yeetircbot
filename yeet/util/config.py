from configparser import RawConfigParser
import re

class Namespace(object):
    def __init__(self):
        pass

class ConfigParser(RawConfigParser):
    def __init__(self):
        super().__init__()
        self.ns = Namespace()


    def getlist(self, section, option):
        tmp = self.get(section, option).strip()
        return re.split(r' +', tmp)

    def getdefault(self, section,  option, default=None, getter=None):
        if not self.has_option(section, option):
            return default
        if not getter:
            getter = self.get
        return getter(section, option)

    def getintdefault(self, section, option, default=None):
        self.getdefault(self, section, option, default, self.getint)

    def getfloatdefault(self, section, option, default=None):
        self.getdefault(self, section, option, default, self.getfloat)

    def getbooldefault(self, section, option, default=None):
        self.getdefault(self, section, option, default, self.getboolean)

    def getlistdefault(self, section, option, default=None):
        self.getdefault(self, section, option, default, self.getlist)
    
    def ns_get(self, section, option, default=None, dest=None, getter=None):
        if not dest:
            dest = option
        if not getter:
            getter = self.get
        setattr(self.ns, dest, self.getdefault(section, option, default, getter))
    
    def ns_getint(self, section, option, default=None, dest=None):
        self.ns_get(section, option, default, dest, self.getint)

    def ns_getfloat(self, section, option, default=None, dest=None):
        self.ns_get(section, option, default, dest, self.getfloat)

    def ns_getbool(self, section, option, default=None, dest=None):
        self.ns_get(section, option, default, dest, self.getboolean)

    def ns_getlist(self, section, option, default=None, dest=None):
        self.ns_get(section, option, default, dest, self.getlist)
