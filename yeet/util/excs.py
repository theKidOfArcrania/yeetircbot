class BotError(Exception):
    pass

class PassThroughError(BotError):
    pass

class BotInvocationError(BotError):
    pass

class CommandError(BotError):
    pass

class CheckError(CommandError):
    pass

class MissingPermissionError(CheckError):
    pass

class BadArgumentError(CommandError):
    pass

class MissingCommandError(CommandError):
    pass

