import shlex, logging
from functools import partial

import irc.bot
import irc.strings
from irc.client import ip_numstr_to_quad, ip_quad_to_numstr

from yeet.cmd.commands import Command, invoke_cmd
from yeet.model import Database
from yeet.util.config import ConfigParser
from yeet.util.loggable import Loggable
from yeet.util.excs import *

class CmdHandler(Loggable):
    def __init__(self, bot, conffile, prefix='.'):
        super(Loggable, self).__init__()

        config = ConfigParser()
        config.read(conffile)
        self.init_config(config)

        self.__prefix = prefix

        bot.connection.add_global_handler('all_events', self.__logevt, -30)
        bot.connection.add_global_handler('all_events', self.__dispatch, -10)

        for i in ['privmsg', 'nicknameinuse', 'welcome', 'pubmsg', 'kick']:
            fn = getattr(self, '_on_' + i)
            bot.connection.add_global_handler(i, partial(self.__exc_handle, 
                fn, self.__handle_err), -17)

        self.bot = bot

    def __logevt(self, c, e):
        if e.type == 'all_raw_messages':
            return
        self.log.debug(e)

    def __exc_handle(self, fn, handler, c, e, *args):
        try:
            try:
                return fn(c, e, *args)
            except BotError:
                raise
            except Exception as exc:
                raise BotInvocationError() from exc
        except:
            handler(c, e)

    def __prep_evt(self, c, e):
        if e.source:
            e.source_user = self.db.identify_user(e.source)
        else:
            e.source_user = None
            

    def __handle_err(self, c, e):
        def defaction(c, e):
            self.log.error('Error while handling "%s"', e.type, exc_info=True)
        getattr(self, 'on_error', defaction)(c, e)

    def __handle_cmderr(self, c, e):
        def defaction(c, e):
            raise
        getattr(self, 'on_cmderror', defaction)(c, e)


    def __dispatch(self, c, e):
        self.log.debug('Dispatching %s', e.type)

        def do_nothing(connection, event):
            return None

        self.__prep_evt(c, e)
        method = getattr(self, "on_" + e.type, do_nothing)
        return self.__exc_handle(method, self.__handle_err, c, e)
    
    def init_config(self, conf):
        super().init_config(conf)

        self.__ns = conf.ns
        conf.ns_getbool('irc', 'reconnect_on_kick', True)
        conf.ns_get('irc', 'nick', 'YeetB0t')
        conf.ns_get('irc', 'realname', self.__ns.nick)
        conf.ns_getlist('irc', 'channels')
        conf.ns_get('db', 'db_path', 'db.dat')

        self.db = Database.db_load(conf.ns.db_path)

    def _on_nicknameinuse(self, c, e):
        self.log.error('Nickname `%s\' in use', c.get_nickname())
        c.nick(c.get_nickname() + "_")

    def _on_welcome(self, c, e):
        for chan in self.__ns.channels:
            self.log.info('Joining %s', chan)
            c.join(chan)

    def _on_kick(self, c, e):
        chan = e.arguments[0]
        self.log.info('%s was kicked from %s', ', '.join(e.arguments), e.target)

        if c.get_nickname() not in e.arguments:
            return

        if self.__ns.reconnect_on_kick:
            self.log.info('Joining %s', e.target)
            c.join(e.target)

    def _on_privmsg(self, c, e):
        e.target = e.source.nick
        cmd = e.arguments[0]
        if cmd.startswith(self.__prefix):
            cmd = cmd[len(self.__prefix):]
        self.__exc_handle(self.__do_command, self.__handle_cmderr, c, e, cmd)

    def _on_pubmsg(self, c, e):
        cmd = e.arguments[0]
        if cmd.startswith(self.__prefix):
            cmdline = cmd[len(self.__prefix):]
            self.__exc_handle(self.__do_command, self.__handle_cmderr, c, e, cmdline)


    def __do_command(self, c, e, cmdline):
        # Parse the command arguments
        try:
            parts = shlex.split(cmdline)
        except:
            raise BadArgumentError()

        self.__prep_evt(c, e)
        invoke_cmd(self, c, e, parts)
            #c.notice(nick, "Not understood: " + cmd)
