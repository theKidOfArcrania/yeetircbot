from .handler import CmdHandler
from yeet.model import Database, Role, RoleAcl, NickMask
from yeet.cmd import commands
from yeet.util.excs import *
import sys

class YeetBouncer(CmdHandler):
    def __init__(self, bot, config='yeet.conf'):
        super(YeetBouncer, self).__init__(bot, config)
        for chan in bot.channels.keys():
            bot.connection.send_items('MODE', chan)

    def init_config(self, conf):
        super().init_config(conf)

        self.__ns = conf.ns
        conf.ns_get('irc', 'yeet_msg', 'yeet')

    def on_join(self, c, e):
        kick = ''
        self.log.info('{} joined'.format(e.source))
        if e.source.nick == c.get_nickname():
            c.send_items('MODE', e.target)
            return

        if not e.source_user:
            kick = 'New fone, who dis?'
            if 'ebeard' in e.source or 'Eve' in e.source:
                kick = 'Unban eve from #yeet7 you cowards!'
        elif not e.source_user.check_perm_name('access'):
            kick = 'You do not have access, :C'
        else:
            c.notice(e.target, 'Welcome to %s, %s!' %(e.target,
                e.source.nick))
            return

        c.kick(e.target, e.source.nick, kick)
        c.notice(e.target, '%s tried to join... :/' % e.source)
    
    def on_channelmodeis(self, c, e):
        e.target = e.arguments[0]
        e.arguments = e.arguments[1:]
        self.bot._on_mode(c, e)

    def on_invite(self, c, e):
        chan = e.arguments[0]
        self.log.info('Invited to %s', chan)
        c.join(chan)

    def on_kick(self, c, e):
        if e.arguments[0] != c.get_nickname():
            return

    def on_pubmsg(self, c, e):
        msg = e.arguments[0]
        if msg.startswith('.'):
            return

        if not (e.source_user and e.source_user.check_perm_name('can_yeet')):
            return

        msg = msg.replace('3', 'e').lower()
        if 'yeet' in msg or 'same' in msg or 'nice' in msg:
            self.log.debug('Yeeting to %s', e.target)
            c.privmsg(e.target, self.__ns.yeet_msg)
        elif 'yeb' in msg:
            c.privmsg(e.target, 'yeb :)')

    def on_cmderror(self, c, e):
        exc = sys.exc_info()[1]
        if isinstance(exc, MissingCommandError):
            msg = 'Invalid command'
        elif isinstance(exc, PassThroughError):
            return
        elif isinstance(exc, (MissingPermissionError, CheckError)):
            msg = 'You do not have permissions'
        elif isinstance(exc, BadArgumentError):
            msg = 'Invalid arguments'
        elif isinstance(exc, CommandError):
            msg = 'Your command was malformed'
        else:
            c.notice(e.target, 'An unexpected error occurred!')
            raise
        
        reason = ''
        if exc.args:
            reason = ': ' + ', '.join(exc.args)
        c.notice(e.target, '%s%s!' % (msg, reason))

    def on_mode(self, c, e):
        bot_nick = c.get_nickname()
        chan = self.bot.channels[e.target]
        
        can_talk = chan.is_voiced(bot_nick) or chan.is_halfop(bot_nick) or \
            chan.is_oper(bot_nick)
        if chan.is_moderated() and not can_talk:
            self.log.info('Reconnecting to %s due to unvoiced issue', e.target)
            c.part(e.target)
            c.join(e.target)

    def save(self):
        self.db.db_store(self.__ns.db_path)

    def load(self):
        self.db = Database.db_load(self.__ns.db_path)

    def __check_perms(self, c, e, *perms):
        if e.source_user:
            for p in perms:
                if not e.source_user.check_perm_name(p):
                    break
            else:
                return True

        raise MissingPermissionError('need ' + ', '.join(perms))

    def __get_user(self, c, e, name):
        try:
            return self.db.get_user(name)
        except:
            c.notice(e.target, 'Invalid user name: "%s"' % name)
            raise PassThroughError()

    @commands.group
    def iden(self, c, e):
        pass

    @iden.command('list')
    def ilist(self, c, e, user):
        idens = self.__get_user(c, e, user).iden_patterns

        c.notice(e.target, 'Identities for %s:' % user)
        for iden in idens:
            c.notice(e.target, ' * %s' % iden)

    @commands.is_user()
    @iden.command('add')
    def iadd(self, c, e, user, iden):
        user = self.__get_user(c, e, user)
        if user != e.source_user:
            self.__check_perms(c, e, 'manage_users')
        user.add_iden(NickMask(iden))
        c.notice(e.target, 'Successfully added identity')
    
    @commands.is_user()
    @iden.command('remove')
    def iremove(self, c, e, user, iden):
        user = self.__get_user(c, e, user)
        if user != e.source_user:
            self.__check_perms(c, e, 'manage_users')
        try:
            user.remove_iden(NickMask(iden))
            c.notice(e.target, 'Successfully removed identity')
        except:
            c.notice(e.target, '%s did not have "%s" identity' % (user, iden))

    @commands.has_permissions('access')
    @commands.command
    def invite(self, c, e, channel):
        self.log.info('Inviting %s to %s', e.source.nick, channel)
        c.invite(e.source.nick, channel)

    @commands.has_permissions('manage_mode')
    @commands.command
    def opme(self, c, e, channel=None, name=None):
        if not channel:
            channel = e.target
        if not name:
            name = e.source.nick

        c.mode(channel, '+o %s' % name)
        c.notice(e.target, 'Mode change')

    @commands.has_permissions('manage_mode')
    @commands.command
    def mode(self, c, e, *args):
        if not args:
            raise BadArgumentError()

        if args[0][0] == '#':
            channel = args[0]
            args = ' '.join(args[1:])
            if not args:
                raise BadArgumentError()
        else:
            channel = e.target
            args = ' '.join(args)

        print(args)
        c.mode(channel, args)
        c.notice(e.target, 'Mode change')

    def __role_at(self, c, e, name, ind_allow_after=False):
        ind_allow_after = not not ind_allow_after
        try:
            ind = int(name)
            if not (0 <= ind < len(self.db.roles) + ind_allow_after):
                c.notice(e.target, 'Index not in range')
                raise PassThroughError()
            return ind
        except:
            if not self.db.is_valid_role(name):
                c.notice(e.target, 'No such role "%s"' % name)
                raise PassThroughError()
            return self.db.get_role_ind(name)

    def __check_modrole(self, c, e, ind):
        user = e.source_user
        if user.check_perm_name('admin'):
            return True
        minind = min(self.__role_at(c, e, role) for role in user.roles)
        if minind < ind:
            return True
        
        raise MissingPermissionError('the role you want to manage is above your own')

    @commands.group
    def roles(self, c, e, subcmd=None):
        if subcmd == None:
            self.rlist.function(self, c, e)
            return 'NO MORE'

    @roles.command('list')
    def rlist(self, c, e):
        c.notice(e.target, 'List of all roles (highest precedence on top):')
        ind = 0
        for r in self.db.roles:
            c.notice(e.target, '%d: %s: [%s]' % (ind, r, ', '.join(r.acl.acl_names)))
            ind += 1

    @commands.has_permissions('manage_roles')
    @commands.is_user()
    @roles.command('add')
    def radd(self, c, e, name, ind=None):
        if ind == None:
            ind = len(self.db.roles)
        ind = self.__role_at(c, e, ind, True)
        self.__check_modrole(c, e, ind)

        try:
            int(name)
            c.notice(e.target, 'Not a valid role name!')
            return
        except:
            if self.db.is_valid_role(name):
                c.notice(e.target, 'Role "%s" already exist!' % name)
                return
        
        role = Role(name)
        RoleAcl(role, 0)
        self.db.roles.insert(ind, role)
        c.notice(e.target, 'Successfully added role')

    @commands.has_permissions('manage_roles')
    @commands.is_user()
    @roles.command('up')
    def rup(self, c, e, name):
        ind = self.__role_at(c, e, name)
        self.__check_modrole(c, e, ind - 1)

        if ind == 0:
            c.notice(e.target, 'Already at the very top of the list!')
            return

        
        tmp = self.db.roles[ind]
        self.db.roles[ind] = self.db.roles[ind-1]
        self.db.roles[ind-1] = tmp
        c.notice(e.target, 'Successfully moved up role')
        
    @commands.has_permissions('manage_roles')
    @commands.is_user()
    @roles.command('down')
    def rdown(self, c, e, name):
        ind = self.__role_at(c, e, name)
        self.__check_modrole(c, e, ind)

        if ind >= len(self.db.roles) - 1:
            c.notice(e.target, 'Already at the very bottom of the list!')
            return
        
        tmp = self.db.roles[ind]
        self.db.roles[ind] = self.db.roles[ind+1]
        self.db.roles[ind+1] = tmp
        c.notice(e.target, 'Successfully moved down role')
    
    @commands.has_permissions('manage_roles')
    @commands.is_user()
    @roles.command('remove')
    def rremove(self, c, e, name):
        ind = self.__role_at(c, e, name)
        self.__check_modrole(c, e, ind)
        self.db.delete_role(name)
        c.notice(e.target, 'Successfully removed role')

    @commands.is_user()
    @roles.command('perm')
    def rperm(self, c, e, name, *setting):
        ind = self.__role_at(c, e, name)
        if len(setting) == 0:
            r = self.db.roles[ind]
            c.notice(e.target, '%s: [%s]' % (r, ', '.join(r.acl.acl_names)))
            return

        self.__check_perms(c, e, 'admin')

        for arg in setting:
            if arg[0] == '+':
                val = 1
            elif arg[0] == '-':
                val = 0
            else:
                c.notice(e.target, 'begin each setting with + or - please!')

            arg = arg[1:]
            try:
                self.db.roles[ind].acl.set_acl(arg, val)
            except:
                self.log.info('Something weird', exc_info=True)
                c.notice(e.target, 'Invalid permission: "%s"' % arg)
        c.notice(e.target, 'Successfully changed role permissions')

    @commands.group
    def users(self, c, e, subcmd=None):
        if subcmd == None:
            self.ulist.function(self, c, e)
            return 'NO MORE'

    @users.command('list')
    def ulist(self, c, e, user=None):
        if user:
            u = self.__get_user(c, e, user)
            c.notice(e.target, repr(u))
        else:
            c.notice(e.target, 'List of users:')
            for u in self.db.users:
                c.notice(e.target, ' * ' + repr(u))

    @commands.has_permissions('manage_users')
    @commands.is_user()
    @users.command('add')
    def uadd(self, c, e, user):
        try:
            self.db.add_user(user)
            c.notice(e.target, 'Successfully added an user')
        except ValueError:
            c.notice(e.target, 'User already exist!')
    
    @commands.has_permissions('manage_users')
    @commands.is_user()
    @users.command('role')
    def urole(self, c, e, user, *roles):
        if not roles:
            self.ulist.function(self, c, e, user)
            return

        user = self.__get_user(c, e, user)
        self.__check_perms(c, e, 'manage_users')
        actions = []
        for arg in roles:
            if arg[0] == '+':
                val = 1
            elif arg[0] == '-':
                val = 0
            else:
                c.notice(e.target, 'begin each argument with + or - please!')
                return
            role = self.__role_at(c, e, arg[1:])
            self.__check_modrole(c, e, role)
            actions.append((self.db.roles[role], val))
        
        for role, adding in actions:
            if adding:
                user.add_role(role)
            else:
                user.remove_role(role)

        c.notice(e.target, 'Successfully modified the roles!')

    @commands.has_permissions('manage_users')
    @commands.is_user()
    @users.command('remove')
    def uremove(self, c, e, user):
        try:
            user.remove_user(NickMask(iden))
            c.notice(e.target, 'Successfully removed user')
        except:
            c.notice(e.target, 'User did not exist!')
    
    @commands.command
    @commands.has_permissions('can_yeet')
    @commands.is_user()
    def yeet(self, c, e, args='asdf'):
        c.privmsg(e.target, 'yeeting')

    @commands.group
    @commands.is_user()
    def testing(self, c, e, arg):
        c.notice(e.target, 'Test: %s' % (self.testing.subcmds))

    @testing.command
    def carry(self, c, e, arg):
        c.notice(e.target, 'carry: ' + arg)
