import enum

from yeet.util.serializer import singleton, fields

@singleton('name')
class Role(object):
    def __init__(self, r):
        if type(r) is str:
            self.__name = r
        else:
            self.__name = r.name
        self.acl = None

    def __eq__(self, other):
        if not isinstance(other, Role):
            return False
        return self.__name == other.__name

    def __str__(self):
        return self.__name

    def __repr__(self):
        return 'Role: %s' % self.__name

    def __hash__(self):
        return 37 * hash(self.__name) + 41

    @property
    def name(self):
        return self.__name

    def post_init(self, database):
        if not self.acl:
            self.acl = database.fetch_role_acl(self)


class _AclTypes(enum.IntEnum):
    access, can_yeet, manage_mode, manage_roles, manage_users, admin = range(6)

@fields
class RoleAcl(object):
    __flds__ = {'role': Role, 'raw_acl': int}
    __slots__ = '__role', 'raw_acl'

    def __init__(self, role, raw_acl):
        self.__role = role
        self.raw_acl = raw_acl
        role.acl = self

    def __str__(self):
        return 'RoleAcl(%s)' % self.role

    def __repr__(self):
        acls = []
        for acl in _AclTypes._member_map_.values():
            if (1 << acl) & self.raw_acl:
                acls.append(acl.name)
        acls = ', '.join(acls) if acls else '<none>'
        return 'RoleAcl(%s): %s' % (self.role, acls)
        
    def __eq__(self, other):
        if not isinstance(other, RoleAcl):
            return False
        return self.raw_acl == other.raw_acl

    def __hash__(self):
        return 37 * hash(self.raw_acl) + 41

    def __getattr__(self, name):
        is_admin = self.raw_acl & (1 << _AclTypes.admin)
        return ((self.raw_acl & (1 << getattr(_AclTypes, name))) | is_admin) != 0

    def __setattr__(self, name, value):
        try:
            self.set_acl(name, value)
        except:
            return super().__setattr__(name, value)


    @property
    def acl_names(self):
        for d in dir(_AclTypes):
            if '__' not in d and self.get_actual_acl(d):
                yield d

    def get_actual_acl(self, name):
        return (self.raw_acl & (1 << getattr(_AclTypes, name))) != 0

    def set_acl(self, name, value):
        if name.startswith('_'):
            raise AttributeError()

        bit = 1 << getattr(_AclTypes, name)
        if value:
            self.raw_acl |= bit
        else:
            self.raw_acl &= ~bit
        

    def __dir__(self):
        super().__dir__() + [d for d in dir(_AclTypes) if '__' not in d]

    @property
    def role(self):
        return self.__role

