from collections import OrderedDict
import fnmatch

from irc.client import NickMask

from yeet.util.serializer import Desc, fields, ListType, register_desc

from .role import Role, RoleAcl

class _NickMaskDesc(Desc):
    def load(self, os, data):
        return NickMask(data)
    def store(self, os, obj):
        return str(obj)
register_desc(NickMask, _NickMaskDesc())

@fields
class User(object):
    __flds__ =  {'name': str, 'iden_patterns': ListType(NickMask), 
            'roles': ListType(Role)}

    def __init__(self, name, iden_patterns = [], roles = []):
        self.__name = name
        self.__idens = dict((NickMask(i), None) for i in iden_patterns)
        self.__roles = OrderedDict((Role(r), None) for r in roles)

    @property
    def effective_acl(self):
        eff = RoleAcl(Role('<effective>'), 0)
        for r in self.__roles:
            eff.raw_acl |= r.acl.raw_acl
        return eff

    @property
    def iden_patterns(self):
        return self.__idens.keys()

    @property
    def name(self):
        return self.__name

    @property
    def roles(self):
        return self.__roles.keys()

    def __bool__(self):
        return True

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        idens = ', '.join(map(str, self.iden_patterns))
        roles = ', '.join(map(str, self.roles))
        return 'User: %s (%s), roles: %s' % (self.name, idens, roles)

    def add_iden(self, iden):
        self.__idens[iden] = None

    def add_role(self, role):
        self.__roles[self.__db.get_role(role)] = None

    def check_perm_name(self, acl_name):
        return bool(getattr(self.effective_acl, acl_name, 0))

    def has_iden(self, iden):
        for patt in self.__idens.keys():
            if fnmatch.fnmatchcase(iden, patt):
                return True
        return False

    def has_role(self, role):
        return Role(role) in self.__roles

    def post_init(self, db):
        self.__db = db
        roles = OrderedDict()
        for r in self.roles:
            roles[db.get_role(r)] = None

        self.__roles.clear()
        self.__roles.update(roles)

    def remove_iden(self, iden):
        del self.__idens[iden]

    def remove_role(self, role):
        role = Role(role)
        if role in self.__roles:
            del self.__roles[role]


