import json
from .user import User
from .role import RoleAcl, Role
from yeet.util.serializer import fields, ListType, Serializer

@fields
class Database(object):
    __flds__ = {'users': ListType(User), 'role_acls': ListType(RoleAcl)}

    @staticmethod
    def db_load(file):
        os = Serializer()
        with open(file, 'r') as f:
            data = json.load(f)
            return os.load(data, Database)

    def __init__(self, users, role_acls):
        self.__roles = list(acl.role for acl in role_acls)
        self.__usersmap = dict((user.name, user) for user in users)

        for u in users:
            u.post_init(self)

    @property
    def role_acls(self):
        return [role.acl for role in self.__roles]

    @property
    def roles(self):
        return self.__roles

    @property
    def users(self):
        return self.__usersmap.values()

    def add_user(self, name):
        if self.has_user(name):
            raise ValueError()
        self.__usersmap[name] = User(name)


    def delete_role(self, role):
        role = Role(role)
        if role in self.__roles:
            self.__roles.remove(role)
            for u in self.__usersmap.values():
                u.remove_role(role)

    def delete_user(self, name):
        del self.__usersmap[name]

    def has_user(self, name):
        return name in self.__usersmap

    def identify_user(self, iden):
        for u in self.__usersmap.values():
            if u.has_iden(iden):
                return u

    def get_role(self, role):
        return self.__roles[self.get_role_ind(role)]

    def get_role_ind(self, role):
        return self.__roles.index(Role(role))

    def get_user(self, name):
        return self.__usersmap[name]
        
    def is_valid_role(self, role):
        return Role(role) in self.__roles

    def db_store(self, file):
        os = Serializer()
        data = json.dumps(os.store(self)) 
        with open(file, 'w') as f:
            f.write(data)
